---
title: "Searching data"
subtitle: 
comments: false
weight: 1500

---

### Overview
**Faceted search** capability, a combination of free text search and context-sensitive filters (facets), is used to search for publically available SDMX Dataflows using a specific language. The following sections of the documentation describe the features of the faceted search within the .Stat Data Explorer.  
A set of data is an **SDMX Dataflow** which is defined by a data source, a structure (with varying dimensions and dimension values), categorisations and constraints for allowable data content. The aim of the faceted search features is to query against the dataflow structures, to index those and allow a search client to retrieve appropriate information through a standard search application interface (in this case the Data Explorer).
