---
title: "Using .Stat Data Explorer"
subtitle: 
comments: false
weight: 1000

---

* [Design principles & functional vision](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/design-principles/)
* [General layout and common features](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/general-layout/)
* [Searching data](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/searching-data/)
* [Viewing data](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/)
* [SDMX annotations supported by the .Stat Suite](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/sdmx-annotations/)
