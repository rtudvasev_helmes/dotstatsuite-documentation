---
title: "Charts"
subtitle: 
comments: 
weight: 3000

---

All charts use the current selection, which can be modified using the filters.  
All charts are customisable, using the common Customise menu.  
All charts are downloadable in png format.  

The charts share a common header and footer with the preview-table. More information is available [here](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/common-header-and-footer/).

*(since [May 19, 2021 Release .Stat Suite JS 8.0.0](https://sis-cc.gitlab.io/dotstatsuite-documentation/changelog/#may-19-2021))* Below each chart, there is a collapsed localised help text with hints on what to do if the generated chart doesn't correspond to the chart that the user has expected.

![Hints](/dotstatsuite-documentation/images/chart-hints.png)  

By expanding this field, the help text indicates specifically for each type of chart how the user can change chart options or data selections in order to design the chart according to her/his needs under the given chart business rules.  
  
The following types of charts are supported:

![Toolbar](/dotstatsuite-documentation/images/de-toolbar-chart.jpg)

More details about our set of configurable charts based on [d3](https://d3js.org/) to be found [here](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-d3-charts/-/blob/master/README.md), and also [chart responsiveness](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-d3-charts/-/blob/master/docs/charts-responsiveness.md).

---

### Bar chart
**Example**  

![bar chart](/dotstatsuite-documentation/images/chart-bar.png)

---

### Row chart
**Example**  

![row chart](/dotstatsuite-documentation/images/chart-row.png)

---

### Stacked bar chart
**Example**  

![stacked bar chart](/dotstatsuite-documentation/images/chart-stacked.png)

---

### Stacked row chart
> Introduced in [December 14, 2021 Release .Stat Suite JS 11.0.0](https://sis-cc.gitlab.io/dotstatsuite-documentation/changelog/#december-14-2021) 

**Example**  

![stacked row chart](/dotstatsuite-documentation/images/chart-stacked-row.jpg)

---

### Scatter plot chart
**Example**  

![scatter plot chart](/dotstatsuite-documentation/images/chart-scatter.png)

---

### Horizontal symbol chart
**Example**  

![horizontal symbol chart](/dotstatsuite-documentation/images/chart-horizontal-symbol.png)

---

### Vertical symbol chart
**Example**  

![vertical symbol chart](/dotstatsuite-documentation/images/chart-vertical-symbol.png)

---

### Timeline chart
**Example**  

![timeline chart](/dotstatsuite-documentation/images/chart-timeline.png)

---

### Choropleth map
**Example**  

![choropleth map](/dotstatsuite-documentation/images/chart-choropleth.png)

