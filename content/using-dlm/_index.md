---
title: "Using .Stat Data Lifecycle Manager"
subtitle: 
comments: false
weight: 200

---

* [DLM product design & functional vision](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/product_vision/)
* [Log in the DLM](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/log-in-dlm/)
* [Manage user access](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-user-access/)
* [DLM homepage overview](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/dlm_overview/)
* [Manage structures](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-structures/)
* [Manage data](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/manage-data/)
* [Export all in dump mode](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-dlm/dump-mode/)
