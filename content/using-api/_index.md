---
title: "Using .Stat Suite APIs"
subtitle: 
comments: false
weight: 4000

---

* [Main APIs features](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/api-main-features/)
* [.Stat Suite Core data model](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/core-data-model/)
* [Referential metadata features](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/ref-metadata/)
* [Typical use cases](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/typical-use-cases/)
* [.Stat RESTful Web Service Cheat Sheet](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/restful/)
* [Embargo management](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/embargo-management/)
* [Email notifications for data management](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/message-through-mail/)
* [Programmatic authentication](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/programmatic-auth/)
